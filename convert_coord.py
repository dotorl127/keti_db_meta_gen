import numpy as np


class convert_coord:
    def __init__(self):
        self.nx = 149
        self.ny = 253
        self.re = 6371.00877
        self.grid = 5.0
        self.slat1 = 30.0
        self.slat2 = 60.0
        self.olon = 126.0
        self.olat = 38.0
        self.xo = 210 / self.grid
        self.yo = 675 / self.grid
        self.first = True

    def map_conv(self, lon, lat):
        if self.first:
            self.re = self.re / self.grid
            self.slat1 = np.deg2rad(self.slat1)
            self.slat2 = np.deg2rad(self.slat2)
            self.olon = np.deg2rad(self.olon)
            self.olat = np.deg2rad(self.olat)

            self.sn = np.tan(np.pi * 0.25 + self.slat2 * 0.5) / np.tan(np.pi * 0.25 + self.slat1 * 0.5)
            self.sn = np.log(np.cos(self.slat1) / np.cos(self.slat2)) / np.log(self.sn)

            self.sf = np.tan(np.pi * 0.25 + self.slat1 * 0.5)
            self.sf = np.power(self.sf, self.sn) * np.cos(self.slat1) / self.sn

            self.ro = np.tan(np.pi * 0.25 + self.olat * 0.5)
            self.ro = self.re * self.sf / np.power(self.ro, self.sn)

            self.first = False

        self.ra = np.tan(np.pi * 0.25 + np.deg2rad(lat) * 0.5)
        self.ra = self.re * self.sf / np.power(self.ra, self.sn)

        theta = np.deg2rad(lon) - self.olon
        if theta > np.pi:
            theta -= 2.0 * np.pi
        if theta < -np.pi:
            theta += 2.0 * np.pi
        theta *= self.sn

        x = float(self.ra * np.sin(theta)) + self.xo
        y = float(self.ro - self.ra * np.cos(theta)) + self.yo

        return int(x + 1.5), int(y + 1.5)