import os
import argparse
import tqdm
from get_data import *
from util_dict import class_dict, refined_weather_dict
import pandas as pd


def parse_args():
    parser = argparse.ArgumentParser(description='arg parser')
    parser.add_argument('--dataset_path', type=str, default='/media/moon/extraDB/KETI_DB_EXTRACT_RET',
                        help='specify the dataset path or directory location')
    parser.add_argument('--excel_path', type=str, default=None,
                        help='specify the excel file path')
    parser.add_argument('--gen_excel', action='store_true')
    args = parser.parse_args()
    return args


def refine_weather(weather_api, weather_ws, anno_weather):
    if weather_api is not None:
        refined_weather_dict['temperature'] = weather_api['temperature']
        refined_weather_dict['precipitation'] = weather_api['precipitation']
        refined_weather_dict['humidity'] = weather_api['humidity']
        refined_weather_dict['wind speed'] = weather_api['wind speed']
        if 'type' in weather_api:
            type = 'fine'
            if 1 <= weather_api['type'] < 3:
                type = 'rain'
            elif weather_api['type'] == 3:
                type = 'snow'
            elif weather_api['type'] == 6:
                type = 'fog'
            refined_weather_dict['type'] = type
        if 'snowfall' in weather_api:
            refined_weather_dict['precipitation'] = weather_api['snowfall']

    if weather_ws is not None:
        refined_weather_dict['temperature'] = weather_ws['temperature']
        refined_weather_dict['precipitation'] = weather_ws['precipitation']
        refined_weather_dict['humidity'] = weather_ws['humidity']
        refined_weather_dict['wind speed'] = weather_ws['wind speed']
        if weather_ws['precipitation type'] == 'No precipitation':
            refined_weather_dict['precipitation type'] = 'fine'
        elif weather_ws['precipitation type'] == 'Unspecified precipitation':
            refined_weather_dict['precipitation type'] = 'fog'
        else:
            refined_weather_dict['precipitation type'] = weather_ws['precipitation type']

    if anno_weather is not None:
        refined_weather_dict['type'] = anno_weather

    for k, v in refined_weather_dict.items():
        if v is None:
            refined_weather_dict[k] = str(v)

    return refined_weather_dict


def main():
    args = parse_args()
    root_path = args.dataset_path.rstrip('/')
    dir_list = sorted(os.listdir(root_path))
    dir_list = [dir_name for dir_name in dir_list if os.path.isdir(root_path + '/' + dir_name)]

    if args.gen_excel:
        col = ['file_name', 'scene',
               'time', 'timeslot', 'season',
               'weather', 'precipitation', 'temperature', 'humidity', 'wind speed',
               'address', 'crosswalk',
               'max_speed', 'min_speed', 'avg_speed',
               'person', 'car', 'truck', 'bus', 'bicycle', 'bike', 'extra_vehicle', 'deer', 'dog']
        df = pd.DataFrame(dict.fromkeys(col, []))
        with pd.ExcelWriter(f'{root_path}/KETIDB.xlsx') as w:
            df.to_excel(w, sheet_name='Sheet1', index=False)
        args.excel_path = f'{root_path}/KETIDB.xlsx'

    assert args.excel_path is not None, 'Not found excel file. please check excel path'
    df = pd.read_excel(args.excel_path, sheet_name='Sheet1')

    db_dir_list = df['file_name'].values
    dir_list = sorted(list(set(dir_list) - (set(db_dir_list) & set(dir_list))))

    with tqdm.trange(0, len(dir_list), desc='directories', leave=True, dynamic_ncols=True) as tbar:
        for dir_idx in tbar:
            dir_name = dir_list[dir_idx]
            anno_dict = {'weather': set(),
                         'scene': set(),
                         'objs': class_dict.copy()}
            time_str, season, timeslot, weather_api, address, crosswalk, weather \
                = None, None, None, None, None, None, None
            avg_speed, max_speed, min_speed = 0., -float("inf"), float("inf")
            dataset_path = f'{root_path}/{dir_name}'
            tbar.set_postfix(dict(dirname=dir_name))

            raw_data_path = f'{dataset_path}/Raw_Data'
            assert os.path.exists(raw_data_path), f'>> Error not found Raw data directory in {dataset_path} directory.'

            gps_path = f'{raw_data_path}/GPS'
            gps_list = sorted(os.listdir(gps_path))
            imu_path = f'{raw_data_path}/IMU'
            imu_list = sorted(os.listdir(imu_path))
            weather_path = f'{raw_data_path}/Weather'
            weather_list = sorted(os.listdir(weather_path))
            anno_path = f'{dataset_path}/Annotation'
            anno_list = sorted(os.listdir(anno_path))

            total_iter = len(imu_list) + len(anno_list) + 9
            pbar = tqdm.tqdm(total=total_iter, desc='collecting metadata', leave=False, dynamic_ncols=True)

            pbar.set_postfix(dict(proc='GPS collecting...'))
            time_str, season, timeslot, weather_api, address, crosswalk \
                = get_data_related_gps(f'{gps_path}/{gps_list[0]}', pbar)
            pbar.update()

            pbar.set_postfix(dict(proc='IMU collecting...'))
            total_n = 0
            for imu in imu_list:
                speed, max_speed_, min_speed_, n = get_data_related_imu(f'{imu_path}/{imu}')
                avg_speed += speed
                total_n += n
                max_speed = max(max_speed, max_speed_)
                min_speed = min(min_speed, min_speed_)
                pbar.update()
            avg_speed /= total_n

            pbar.set_postfix(dict(proc='WS collecting...'))
            if weather_list:
                weather = get_weather_ws(f'{weather_path}/{weather_list[0]}')
            pbar.update()

            pbar.set_postfix(dict(proc='Annotation collecting...'))
            for anno in anno_list:
                get_data_related_annotation(f'{anno_path}/{anno}', anno_dict)
                pbar.update()

            pbar.set_postfix(dict(proc='Refine weather metadata...'))
            refined_weather = refine_weather(weather_api, weather, anno_dict['weather'])
            excel_data = [dir_name,
                          ', '.join(list(anno_dict['scene'])),
                          time_str, timeslot, season,
                          ', '.join(list(refined_weather['type'])),
                          refined_weather['precipitation'],
                          refined_weather['temperature'],
                          refined_weather['humidity'],
                          refined_weather['wind speed'],
                          address.rstrip(' '),
                          str(crosswalk),
                          round(max_speed, 3), round(min_speed, 3), round(avg_speed, 3)]
            class_count = [v for k, v in anno_dict['objs'].items()]
            excel_data += class_count
            df.loc[len(df)] = excel_data
            with pd.ExcelWriter(args.excel_path) as w:
                df.to_excel(w, sheet_name='Sheet1', index=False)

            pbar.update()
            pbar.close()


if __name__ == '__main__':
    main()
