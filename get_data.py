import time
import json
import requests
from util_dict import api_dict, weather_shortTerm_category_dict, weather_longTerm_category_dict, loc_code_dict
from convert_coord import convert_coord
import math
import re
gps2coord = convert_coord()


def get_season(month):
    season = None

    if month in (3, 4, 5):
        season = 'Spring'
    elif month in (6, 7, 8):
        season = 'Summer'
    elif month in (9, 10, 11):
        season = 'Fall'
    elif month in (12, 1, 2):
        season = 'Winter'

    return season


def get_timeslot(hour):
    timeslot = None

    if 4 <= hour < 7:
        timeslot = 'Morning Twilight'
    elif 7 <= hour < 12:
        timeslot = 'Morning'
    elif 12 <= hour < 17:
        timeslot = 'After Noon'
    elif 17 <= hour < 21:
        timeslot = 'Evening'
    elif 21 <= hour < 24:
        timeslot = 'Night'
    elif 24 <= hour < 4:
        timeslot = 'Midnight'

    return timeslot


def get_weather_shortTerm_api(lon, lat, time):
    date = f'{time.tm_year}{time.tm_mon:02d}{time.tm_mday:02d}'
    x, y = gps2coord.map_conv(lon, lat)
    url = f'http://apis.data.go.kr/1360000/VilageFcstInfoService_2.0/getUltraSrtNcst'
    params = {
        'serviceKey': api_dict['data_go_dc'],
        'pageNo': '1', 'numOfRows': '10', 'dataType': 'JSON',
        'base_date': date, 'base_time': '0600', 'nx': x, 'ny': y
    }
    res = requests.get(url, params=params)
    if 'SERVICE_KEY_IS_NOT_REGISTERED_ERROR' in str(res.content):
        print('>> Service Key Error. Please check the service key.')
    data = res.json()

    if data['response']['header']['resultCode'] == '00':
        weather = {}
        for d in data['response']['body']['items']['item']:
            if d['category'] in weather_shortTerm_category_dict:
                weather[weather_shortTerm_category_dict[d['category']]] = float(d['obsrValue'])
        return weather
    return None


def get_weather_longTerm_api(time, loc_str):
    weather, loc = None, None
    if loc_str.split(' ')[0] in loc_code_dict:
        loc = loc_code_dict[loc_str.split(' ')[0]]
    elif loc_str.split(' ')[1] in loc_code_dict:
        loc = loc_code_dict[loc_str.split(' ')[1]]
    elif loc_str.split(' ')[1][:2] in loc_code_dict:
        loc = loc_code_dict[loc_str.split(' ')[1][:2]]
    elif loc_str.split(' ')[1][:3] in loc_code_dict:
        loc = loc_code_dict[loc_str.spilt(' ')[1][:3]]
    else:
        return weather

    if loc is not None:
        date = f'{time.tm_year}{time.tm_mon:02d}{time.tm_mday:02d}'
        url = f'http://apis.data.go.kr/1360000/AsosHourlyInfoService/getWthrDataList'
        params = {
            'serviceKey': api_dict['data_go_dc'],
            'pageNo': '1', 'numOfRows': '10', 'dataType': 'JSON', 'dataCd': 'ASOS', 'dateCd': 'HR',
            'startDt': date, 'startHh': f'{time.tm_hour:02d}', 'endDt': date, 'endHh': f'{time.tm_hour: 02d}',
            'stnIds': loc
        }
        res = requests.get(url, params=params)
        if 'SERVICE_KEY_IS_NOT_REGISTERED_ERROR' in str(res.content):
            print('>> Service Key Error. Please check the service key.')
        data = res.json()

        if data['response']['header']['resultCode'] == '00':
            weather = {}
            d = data['response']['body']['items']['item'][0]
            for key, val in d.items():
                if key in weather_longTerm_category_dict:
                    if val == '': val = 0.
                    weather[weather_longTerm_category_dict[key]] = float(val)

    return weather


def get_address_api(lon, lat):
    url = 'https://dapi.kakao.com/v2/local/geo/coord2address.json'
    headers = {'Authorization': f'KakaoAK {api_dict["address"]}'}
    params = {
        'x': lon,
        'y': lat,
        'input_coord': 'WGS84'
    }
    res = requests.get(url, params=params, headers=headers)

    if res.status_code == 200:
        return json.dumps(res.json()['documents'][0]['address']['address_name'], ensure_ascii=False)
    return None


def get_crosswalk_api(lon, lat):
    url = f'http://api.data.go.kr/openapi/tn_pubr_public_crosswalk_api?' \
          f'serviceKey={api_dict["data_go_ec"]}&' \
          f'pageNo=1&numOfRows=1&type=json&' \
          f'longitude={lon}&latitude={lat}'
    res = requests.get(url)
    data = res.json()

    if data['response']['header']['resultCode'] == '00':
        return True
    elif data['response']['header']['resultCode'] == '03':
        return False


def get_data_related_gps(path, pbar):
    time_str, season, timeslot, weather, address, crosswalk = None, None, None, None, None, None

    with open(path, 'r') as f:
        line = f.readline().split()
        if line == '':
            return time_str, season, timeslot, weather, address, crosswalk

        lat, lon = float(line[1]), float(line[2])
        time_ = time.localtime(int(line[0]) / (10 ** 9))

        if time_str is None:
            time_str = f'{time_.tm_year}{time_.tm_mon:02d}{time_.tm_mday:02d}-' \
                       f'{time_.tm_hour:02d}:{time_.tm_min:02d}:{time_.tm_sec:02d}'

            pbar.set_postfix(dict(proc='GPS season'))
            season = get_season(time_.tm_mon)
            pbar.update()

            pbar.set_postfix(dict(proc='GPS timeslot'))
            timeslot = get_timeslot(time_.tm_hour)
            pbar.update()

            pbar.set_postfix(dict(proc='GPS short-term weather'))
            weather = get_weather_shortTerm_api(lon, lat, time_)
            pbar.update()

            pbar.set_postfix(dict(proc='GPS address'))
            address = re.sub(r"[^가-힣\s]", '', get_address_api(lon, lat))
            pbar.update()

            if weather is None:
                pbar.set_postfix(dict(proc='GPS long-term weather'))
                weather = get_weather_longTerm_api(time_, address)
            pbar.update()

            pbar.set_postfix(dict(proc='GPS crosswalk'))
            crosswalk = get_crosswalk_api(lon, lat)
            pbar.update()

    return time_str, season, timeslot, weather, address, crosswalk


def get_data_related_imu(path):
    speed, max_speed, min_speed = 0., -float("inf"), float("inf")
    with open(path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            speed += math.sqrt(float(line[1])**2 + float(line[2])**2 + float(line[3])**2)
            max_speed = max(max_speed, speed)
            min_speed = min(min_speed, speed)
    return speed, max_speed, min_speed, len(lines)


def get_weather_ws(path):
    weather = {}

    with open(f'{path}', 'r') as f:
        lines = f.readlines()
        for line in lines:
            name, val = line.strip().split(" : ")[0], line.strip().split(" : ")[1]

            if name == 'temperature':
                weather[name] = float(val)

            if name == 'humidity':
                weather[name] = float(val)

            if name == 'precipitation':
                weather[name] = float(val)

            if name == 'precipitation type':
                if val == '0':
                    weather[name] = 'No precipitation'
                elif val == '40':
                    weather[name] = 'Unspecified precipitation'
                elif val == '60':
                    weather[name] = 'Rain'
                elif val == '70':
                    weather[name] = 'Snow'

            if name == 'precipitation_intensity':
                weather[name] = float(val)

            if name == 'wind speed':
                weather[name] = float(val)

    return weather


def get_data_related_annotation(path, anno_dict):
    with open(path) as f:
        js = json.load(f)

    anno_dict['weather'].add(js['weather'])
    anno_dict['scene'].add(js['scene'])

    if js['boundingCnt'] > 0:
        objs = js['data']

        for obj in objs:
            if obj['category'][0]['id'] in anno_dict['objs']:
                anno_dict['objs'][obj['category'][0]['id']] += 1
            else:
                anno_dict['objs'][obj['category'][0]['id']] = 1
