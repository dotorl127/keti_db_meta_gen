## KETI_DB_META_GEN

### Requirements
- **python package**
```commandline
pip install tqdm requests xlrd openpyxl pandas
```
- **API**
  - [기상청 초단기 실황](https://www.data.go.kr/data/15084084/openapi.do)
  - [카카오 로컬](https://developers.kakao.com/docs/latest/ko/local/dev-guide#coord-to-address)
  - [전국횡단보도표준데이터](https://www.data.go.kr/data/15028201/openapi.do#/tab_layer_detail_function)

### Arguments
- --dataset_path : dataset location or root directory containing dataset bundles
- --excel_path : excel file location to read/write metadata
- --gen_excel : default excel file generate parameter(store true) 

### Description
When KETI DB directory like below.
```
[DB root directory]
└─[DB directory]
   ├─ Camera
   │  ├─ Camera_name#1
   │  │  ├─ Camera_name#1_000000.jpg
   │  │  ├─ ...
   │  │  └─ Camera_name#1_n.jpg
   │  ├─ ...
   │  └─ Camera_name#N
   ├─ GPS
   │  └─ GPS_name#1
   │     ├─ GPS_name#1_000000.txt
   │     ├─ ...
   │     └─ GPS_name#1_n.txt
   ├─ IMU
   │  └─ IMU_name#1
   │     ├─ IMU_name#1_000000.txt
   │     ├─ ...
   │     └─ IMU_name#1_n.txt
   ├─ LiDAR
   │  ├─ LiDAR_name#1
   │  │  ├─ LiDAR_name#1_000000.bin
   │  │  ├─ ...
   │  │  └─ LiDAR_name#1_n.bin
   │  └─ LiDAR_name#N
   │     ├─ LiDAR_name#N_000000.bin
   │     ├─ ...
   │     └─ LiDAR_name#N_n.bin
   ├─ Radar
   │  ├─ Radar_name#1
   │  │  ├─ Radar_name#1_000000.txt
   │  │  ├─ ...
   │  │  └─ Radar_name#1_n.txt
   │  └─ Radar_name#N
   │     ├─ Radar_name#N_000000.txt
   │     ├─ ...
   │     └─ Radar_name#N_n.txt
   ├─ extra_sensors
   │  └─ extra_sensors_name#1
   │     ├─ extra_sensors_name#1_000000.{ext}
   │     ├─ ...
   │     └─ extra_sensors_name#1_n.{ext}
   └─ Sync
      ├─ sync_000000.txt
      ├─ ...
      └─ sync_n.txt
```
Excute generate metadata code type command below
```
$python KETI_DB_meta_gen.py --dataset_path {dataset location} --excel_path {DB excel file location} 
```

after process you can see metadata below
- **related GPS data**
  - address
  - scene : city, countryside, highway
  - time : YYYYMMDD-hh:mm:ss
  - timeslot : Morning Twilight, Morning, After Noon, Evening, Night, Midnight
  - season : Spring, Summer, Fall, Winter
  - weather : Fine, Cloud, Fog, Rain, Snow
  - crosswalk existence : True/False
- **related sensor data**
  - weather
    - precipitation : mm/h
    - temperature : ℃
    - humidity : %
    - wind speed : km/h
  - vehicle speed
- **related annotation data**
  - object statistics
